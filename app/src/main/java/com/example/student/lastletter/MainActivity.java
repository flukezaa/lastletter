package com.example.student.lastletter;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DatabaseScore mHelper;
    private SQLiteDatabase mDb;
    private Cursor mCursor;
    public final String getStatus = "select * from BGMstatus ";
    private Button reset;
    private String status="";
    Setting setting = new Setting();
    Switch bgmswitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bgmswitch=(Switch)findViewById(R.id.bgmswitch);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mHelper = new DatabaseScore(this);
        mDb = mHelper.getWritableDatabase();
        mCursor = mDb.rawQuery(getStatus,null);
        if (mCursor != null && mCursor.moveToFirst()){
            status=mCursor.getString(0);
        }
        if(status.equals("true")) {
            startService(new Intent(this, BGM.class));
        }

    }

    public void selectMenu(View view){

        String selectedButton;

        selectedButton=((Button)view).getText().toString();
        if(selectedButton.equals("Singleplayer")) {
            Intent singlePlayer = new Intent(this, SinglePlayer.class);
            startActivity(singlePlayer);
            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
        }
        else if(selectedButton.equals("Multiplayer")) {
            Intent multiPlayer = new Intent(this,Lobby.class);
            startActivity(multiPlayer);
        }
        else if(selectedButton.equals("Scoreboard")) {
            Intent scoreBoard = new Intent(this, ScoreBoard.class);
            startActivity(scoreBoard);
        }
        else if(selectedButton.equals("Setting")) {
            Intent setting = new Intent(this, Setting.class);
            startActivity(setting);
        }
    }

    @Override
    public void onPause(){
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                stopService(new Intent(this,BGM.class));
            }
        }
        super.onPause();

    }

    @Override
    public void onResume(){
        if(status.equals("true")) {
            startService(new Intent(this, BGM.class));
        }
        super.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mHelper.close();
        stopService(new Intent(this,BGM.class));
    }

}
