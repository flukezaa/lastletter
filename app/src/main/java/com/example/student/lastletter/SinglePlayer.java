package com.example.student.lastletter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SinglePlayer extends AppCompatActivity {
    public Set dict = new HashSet<String>();
    private EditText inputText;
    private TextView nextLetter;
    private TextView singleScore;
    private ImageView clock;
    private Button sendWordButton;
    private String prevWord = "";
    private ArrayList usedWord = new ArrayList();
    private TextView time;
    public CountDownTimer countTime;
    private long secondleft;
    private Dialog pauseGame;
    private static int score=0;
    private ListView historyword;
    private ArrayAdapter<String> collectedWord;
    private String nameInput;
    private DatabaseScore mHelper;
    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        readDict("engDict.bin");
        clock = (ImageView)findViewById(R.id.clock);
        sendWordButton = (Button)findViewById(R.id.SendWord);
        nextLetter = (TextView)findViewById(R.id.nextLetter);
        singleScore = (TextView)findViewById(R.id.singleScore);
        sendWordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readInput();
            }
        });
        pauseGame = new Dialog(this);
        historyword = (ListView)findViewById(R.id.historyword);
        time =(TextView) findViewById(R.id.time);
        score=0;//fixed score if they use previous one
        mHelper = new DatabaseScore(this);
        mDb = mHelper.getWritableDatabase();
        timeGame();
    }

    private void timeGame(){// set timer of the game
        clock.setImageResource(R.drawable.alarm_clock);
        time.setTextColor(Color.BLACK);
        time.setTypeface(Typeface.DEFAULT);
        countTime = new CountDownTimer(11000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                secondleft = millisUntilFinished / 1000;
                if(secondleft==1){// change color when almost timeup
                    clock.setImageResource(R.drawable.alarm_clock1);
                    time.setTextColor(Color.RED);
                    time.setTypeface(Typeface.DEFAULT_BOLD);
                }
                time.setText(Long.toString(secondleft));
        }

            @Override
            public void onFinish() {
                time.setText("0");
                gameOver();
            }
        };
        countTime.start();
    }

    private void gameOver(){
        countTime.cancel();
        time.setText("0");

        AlertDialog gameoverDialog = new AlertDialog.Builder(this).create();
        gameoverDialog.setCanceledOnTouchOutside(false); //prevent user click other place from dialog
        gameoverDialog.setCancelable(false); //prevent user use go back button to continue game
        gameoverDialog.setTitle("Game Over");
        gameoverDialog.setMessage("\nYour score is "+score+"\n\nPlease enter your name");
        final EditText name = new EditText(this);
        name.setWidth(300);
        name.setGravity(Gravity.CENTER);
        gameoverDialog.setView(name);
        gameoverDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) { //after click ok button, it will link to scoreboard page
                        nameInput = name.getText().toString();
                        dialog.dismiss();
                        sendScore();
                        endGame();
                    }
                });

        gameoverDialog.show();
    }

    public void endGame(){
        Intent scoreBoard = new Intent(this, ScoreBoard.class);
        startActivity(scoreBoard);
        this.finish(); //prevent user go back to game activity
    }

    public void showPause(View view){
        pauseGame.setContentView(R.layout.pause);
        String selectedButton = ((Button)view).getText().toString();

        if(selectedButton.equals("restart")) {
            countTime.cancel();
            Intent singlePlayer = new Intent(this, SinglePlayer.class);
            singlePlayer.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //prevent user goback
            startActivity(singlePlayer);
            finish();
        }
        else if(selectedButton.equals("exit")) {
            countTime.cancel();//end counttime
            Intent mainmenu = new Intent(this, MainActivity.class)
            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainmenu);
            finish();
        }
        Button closepause = (Button) pauseGame.findViewById(R.id.closepause);
        closepause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame.dismiss();
            }
        });
        Button resume = (Button) pauseGame.findViewById(R.id.resume);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseGame.dismiss();
            }
        });
        pauseGame.show();
        if(secondleft==0){// prevent go back while gameover with pause dialog
            pauseGame.cancel();
        }
    }

    private void sendScore(){
        //write the score to database
        //String androidName = android.os.Build.MODEL;
        if(nameInput.isEmpty()){//if user don't fill in the name set default to "Player"
            nameInput="Player";
        }
        //input score and name to database
        mDb.execSQL("INSERT INTO " + DatabaseScore.TABLE_NAME + " (" + DatabaseScore.COL_NAME + ", "
                + DatabaseScore.COL_SCORE + ") VALUES ("+ "'" + nameInput + "'" + ",  " + score + ");");
    }

    private void readDict(String fileName){
        String text = "";
        try{
            InputStream is = getAssets().open(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while(line!=null){
                dict.add(line);
                line = reader.readLine();
            }
            is.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    private String getLastLetter(String word){
        return word.substring(word.length()-1);
    }

    private boolean compareFirstLastLetter(String prevWord,String newWord){
        if(prevWord.equals("")){
            usedWord.add(0,inputText.getText().toString().toLowerCase());
            countTime.cancel();
            timeGame();
            return true;
        }
        String prevLastLetter = getLastLetter(prevWord);
        String newFirstLetter = newWord.substring(0,1);

        if(prevLastLetter.equals(newFirstLetter)){
            usedWord.add(0,inputText.getText().toString().toLowerCase());
            countTime.cancel();
            timeGame();
            return true;
        }
        return false;
    }
    private void readInput(){
        inputText = (EditText) findViewById(R.id.inputText);
        String inputTextCorrection = inputText.getText().toString().toLowerCase();
        //if some people just click without adding anything
        if(inputTextCorrection.equals("")){
            return;
        }
        String nextNewLetter = getLastLetter(inputTextCorrection);

        if(usedWord.contains(inputTextCorrection)) {
            nextLetter.setText("You have used this word. Try again with "+ getLastLetter(prevWord));
            inputText.setText("");
        }
        else if (dict.contains(inputTextCorrection) && compareFirstLastLetter(prevWord, inputTextCorrection)) {// if inputText exist in dict and first letter match with previous

            prevWord = inputTextCorrection;
            nextLetter.setText("WELL DONE! Your next Letter is " + nextNewLetter);
            int sizeScore = inputTextCorrection.length();
            int wordScore=sizeScore*100;
            score=Integer.parseInt(singleScore.getText().toString())+wordScore;
            singleScore.setText(Integer.toString(score));
            addUsedWord();
            inputText.setText("");

        }
        else {
            //if wrong do
            if (prevWord.equals("")) //no intial word when starting the game
            {
                nextLetter.setText("Please use word in dictionary");
            }
            else //
            {
                nextLetter.setText("NOPE! Please try again "+ getLastLetter(prevWord));
            }
            inputText.setText("");
        }
    }

    private void addUsedWord(){
        collectedWord = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,usedWord){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(Color.BLACK);
                textView.setTypeface(null, Typeface.BOLD_ITALIC);
                return textView;
            }
        };
        historyword.setAdapter(collectedWord);
    }

    @Override
    public void onBackPressed() { //prevent back to mainmenu but timer still count caused crash
        countTime.cancel();
        Intent main = new Intent(this, MainActivity.class)
        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
        finish();
    }
}
