package com.example.student.lastletter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class Setting extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    Switch bgmswitch;
    private DatabaseScore mHelper;
    private SQLiteDatabase mDb;
    private Cursor mCursor;
    private Button reset;
    public String state="";
    public final String updateStatusT = "update BGMstatus set status = 'true'";
    public final String updateStatusF = "update BGMstatus set status = 'false'";
    public final String getStatus = "select * from BGMstatus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bgmswitch = findViewById(R.id.bgmswitch);
        mHelper = new DatabaseScore(this);
        mDb = mHelper.getWritableDatabase();
        reset = findViewById(R.id.reset);
        mCursor = mDb.rawQuery(getStatus,null);
        if (mCursor != null && mCursor.moveToFirst()){
            state=mCursor.getString(0);
        }
        bgmswitch.setOnCheckedChangeListener(this);
        bgmswitch.setChecked(Boolean.parseBoolean(state));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            startService(new Intent(this, BGM.class));
            state="true";
            mDb.execSQL(updateStatusT);
        } else {
            stopService(new Intent(this, BGM.class));
            state="false";
            mDb.execSQL(updateStatusF);
        }
    }

    public void resetScore(View v){
        showDialog();
    }

    public void showDialog(){
        AlertDialog.Builder resetDialog = new AlertDialog.Builder(this);
        resetDialog.setCancelable(true);
        resetDialog.setTitle("Reset the score");
        resetDialog.setMessage("Are you sure?");
        resetDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) { //after click ok button, it will link to scoreboard page
                        mDb.execSQL("drop table Score");//delete table in database
                        mHelper.onCreate(mDb);
                        Toast.makeText(getApplicationContext(),"All score is successfully deleted."
                                , Toast.LENGTH_SHORT).show();
                    }
                });
        resetDialog.setNegativeButton( "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) { //after click ok button, it will link to scoreboard page
                        dialog.dismiss();
                    }
                });
        resetDialog.show();
    }

}
