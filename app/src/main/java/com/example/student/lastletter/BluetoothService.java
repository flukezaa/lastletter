package com.example.student.lastletter;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class BluetoothService extends Service{
    BluetoothAdapter bluetoothAdapter;
    static BluetoothDevice[] btArray;
    static Set<BluetoothDevice> bt = new HashSet<BluetoothDevice>();
    static List<String> stringArrayList = new ArrayList<String>();// use with monitorbluetooth
    SendReceive sendReceive;
    MonitorBluetooth monitor;
    private final IBinder mBinder = new LocalBinder();
    private boolean flagReceive = false;
    static final int STATE_LISTENING=1;
    static final int STATE_CONNECTING=2;
    static final int STATE_CONNECTED=3;
    static final int STATE_CONNECTION_FAILED=4;
    static final int STATE_MESSAGE_RECEIVED=5;
    private boolean otherWin = false;
    static final String APP_NAME = "Last_Letter";
    //static final UUID MY_UUID = UUID.randomUUID();//create a random uuid
    static final UUID MY_UUID =  UUID.fromString("c09374f0-40f7-4c38-9210-140218dabed0");

    private BluetoothDevice btDeviceClient = null;
    private BluetoothAdapter btDeviceServer = null;

    public class LocalBinder extends Binder {
        public BluetoothService getService(){
            return BluetoothService.this;
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("BluetoothService","onStartCommand!");
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        monitor=new MonitorBluetooth(getApplicationContext());
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"This device not support bluetooth",Toast.LENGTH_SHORT).show();
        } else {
//            if (!bluetoothAdapter.isEnabled()) {
//                enableDeviceDiscovery();
//            }
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        monitor.stopWorking();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //service start due to call with bindService
        return mBinder;
    }
    public void serverStart(){
        btDeviceServer = bluetoothAdapter;
        ServerClass serverClass = new ServerClass();
        serverClass.start();
    }
    public void clientStart(int position){
        btDeviceClient = btArray[position];
        ClientClass clientClass = new ClientClass(btArray[position]);
        clientClass.start();
    }
    public void scanStart(){
        bt.clear();
        monitor.start();
        //get all pair device
        bt.addAll(bluetoothAdapter.getBondedDevices());
    }
    public BluetoothDevice getBtClientDevice(){
        return btDeviceClient;
    }
    public BluetoothAdapter getBtServerDevice(){
        return btDeviceServer;

    }
//    private void enableDeviceDiscovery(){
//        AlertDialog enableDiscoveryDiag = new AlertDialog.Builder(this).create();
//        enableDiscoveryDiag.setCanceledOnTouchOutside(false); //prevent user click other place from dialog
//        enableDiscoveryDiag.setCancelable(false); //prevent user use go back button to continue game
//        enableDiscoveryDiag.setMessage("Turn on Bluetooth?");
//        enableDiscoveryDiag.setButton(AlertDialog.BUTTON_POSITIVE, "Just do", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intentEnableBt = new Intent((BluetoothAdapter.ACTION_REQUEST_ENABLE));
//                startActivityForResult(intentEnableBt,REQUEST_ENABLE_BLUETOOTH);
//                Toast.makeText(getApplicationContext(),"enabled",Toast.LENGTH_SHORT).show();
//            }
//        });
//        enableDiscoveryDiag.setButton(AlertDialog.BUTTON_NEGATIVE, "SHUT UP", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
//                startActivity(mainActivity);
//            }
//        });
//        enableDiscoveryDiag.show();
//
//    }

Handler handler = new Handler(new Handler.Callback() {
    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what){
            case STATE_LISTENING:
                break;
            case STATE_CONNECTING:
                Toast.makeText(getApplicationContext(),"Connecting",Toast.LENGTH_SHORT).show();
                break;
            case STATE_CONNECTED:
                Toast.makeText(getApplicationContext(),"Connected",Toast.LENGTH_SHORT).show();
                Log.d("Bluetooth","Connected");
                Lobby.btnStart.setEnabled(true);
                break;
            case STATE_CONNECTION_FAILED:
                Toast.makeText(getApplicationContext(),"Connection Failed",Toast.LENGTH_SHORT).show();
                break;
            case STATE_MESSAGE_RECEIVED:
                byte[] readBuff = (byte[])msg.obj;
                String tempMsg = new String(readBuff,0,msg.arg1);
                if(tempMsg.equals("I lose")){
                    Multiplayer.iWin = true;
                }
                else{
                //get message from word message
                //add word
                Multiplayer.usedWord.add(tempMsg);
                Multiplayer.sendWordButton.setEnabled(true);//unlock send button
                Multiplayer.myTurn = true;
                Multiplayer.collectedWord.notifyDataSetChanged();
                Intent intent = new Intent(Multiplayer.RECEIVER_INTENT);
                intent.putExtra(Multiplayer.RECEIVER_MESSAGE,true);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                //readMessage.setText(tempMsg);
                break;
                }
        }
        return true;
    }
});
    public boolean getFlagReceive(){
        return flagReceive;
    }
    public void setFlagReceive(boolean temp){
        flagReceive = temp;
    }
    private class ServerClass extends Thread{
    private BluetoothServerSocket serverSocket;
    public ServerClass(){
        try {
            serverSocket=bluetoothAdapter.listenUsingRfcommWithServiceRecord(APP_NAME,MY_UUID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void run(){
        BluetoothSocket socket = null;
        while(socket == null){
            try {
                Message message = Message.obtain();
                message.what = STATE_CONNECTING;
                handler.sendMessage(message);
                socket=serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
            }
            if(socket!=null){
                Message message = Message.obtain();
                message.what=STATE_CONNECTED;
                handler.sendMessage(message);

                //write code on send and receive
                sendReceive = new SendReceive(socket);
                sendReceive.start();

                break;
            }
        }
    }
}
    private class ClientClass extends Thread{
        private BluetoothDevice device;
        private BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1){
            device = device1;
            try {
                socket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        public void run(){
            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                handler.sendMessage(message);

                sendReceive = new SendReceive(socket);
                sendReceive.start();
            }
            catch (IOException e) {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
    public class SendReceive extends Thread{
        private final BluetoothSocket bluetoothSocket;
        private final InputStream inputStream;
        private final OutputStream outputStream;

        public SendReceive(BluetoothSocket socket)
        {
            bluetoothSocket = socket;
            InputStream tempIn = null;
            OutputStream tempOut = null;

            try {
                tempIn = bluetoothSocket.getInputStream();
                tempOut = bluetoothSocket.getOutputStream();

            } catch (IOException e) {
                e.printStackTrace();
            }
            inputStream = tempIn;
            outputStream = tempOut;
        }
        public void run(){
            byte[] buffer = new byte[1024];
            int bytes;

            while(true){
                try {
                    bytes = inputStream.read(buffer);
                    handler.obtainMessage(STATE_MESSAGE_RECEIVED,bytes,-1,buffer).sendToTarget();;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public void write(byte[] bytes){
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
