package com.example.student.lastletter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseScore extends SQLiteOpenHelper {
    private static final String DB_NAME = "LastLetterScore";
    private static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "Score";

    public static final String COL_NAME = "name";
    public static final String COL_SCORE = "score";
    public static final String BGMstatus = "create table if not exists BGMstatus(status text)";
    public static final String insertStatus = "insert into BGMstatus(status) values ('true')";

    public DatabaseScore(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COL_NAME + " TEXT, " + COL_SCORE + " INT);");
        for(int i=1;i<=10;i++) {
            String text = "Player"+i;
            db.execSQL("INSERT INTO Score(name,score) VALUES ('"+text+"',0)");
        }
        db.execSQL(BGMstatus);
        db.execSQL(insertStatus);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
