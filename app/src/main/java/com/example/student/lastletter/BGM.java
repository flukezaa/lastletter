package com.example.student.lastletter;


import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class BGM extends Service {

    MediaPlayer player;

    @Override
    public IBinder onBind(Intent arg0) {

        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.mindbender);
        player.setLooping(true); // Set looping
        player.start();
    }

    public void onPause(){
        player.stop();
    }

    public void onStop(){
        if (player != null && player.isPlaying()) {
            player.stop();
            player.release();
            player = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.stop();
        player.release();
    }
}